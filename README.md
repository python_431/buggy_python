# Buggy_Python

Des programmes Python qui ne marchent pas

Voici trois programmes Python qui ne marchent pas !

- additionne.py
- salutations.py
- mots.py

Les deux premiers sont complets mais ont des erreurs de
syntaxe ou des problèmes à l'exécution.

Le dernier est à compléter, mais il a peut-être un bug 
après.

Pour chaque erreur, indiquez le nom de l'erreur que Python
affiche, expliquez clairement d'où elle vient et comment
vous pouvez la corriger, et ainsi de suite.

Quand chaque programme s'exécute, vérifiez s'il fonctionne
bien. Si ce n'est pas le cas déterminez pourquoi et comment
corriger le problème.

Le plus simple: juste avant la ligne où il y avait un
problème décrivez le et indiquez comment vous l'avez
résolu dans des commentaires (lignes débutant par #), 
ou bien si vous préférez dans un fichier REPONSE.md
que vous créez et ajoutez ici dans votre branche.
